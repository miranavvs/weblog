<?php
defined ('BASEPATH') OR exit ('No direct script access allowed');

//////////////////////////////||
// CREATION D'UN ARTICLE      ||
//////////////////////////////||
class Create_article extends CI_Model{

    private $_idarticle;
    private $_title;
    private $_alias;
    private $_content;
    private $_picture;
    private $_date_create;
    private $_status;
    private $_idlog;

    public function __construct(){
        parent::__construct();
        $this->clear_data();

    }

    public function __get($key) {
        $method_name = 'get_property_'.$key;
        if ( method_exists ( $this,$method_name ) ){
            return $this->$method_name();
        }else{
            return parent::__get($key);
        }

    }





}