<?php
defoned('BASEPATH') OR exit('No_direct_script_access_allowed');
class Article_status{

    protected $_status ;

    //fonction pour definir l'état de publication
    public function __construct(){
        $this->_status = [
            'W' => [
                'text' => 'Brouillon',
                'decorarion' => 'warning'
            ],
            'P'=> [
                'text' => 'Publié',
                'decoration' => 'primary'
            ],
            'D' => [
                'text' => 'Supprimé',
                'decoration' => 'danger'
            ]
        ] ;
    }

    
    public function __get($key){
        $method_name = 'get_propert_'. $key;
        if(method_exists($this , $method_name)){
            return $this->$method_name();
        } else {
            return parent::__get($key);
        }
    }

    //methode d'affichage 
    protected function get_property_label(){
        $result = [];
        foreach ($this->_status as $key => $values){
            $result[$key] = '<span class ="label label-'.$values['decoration'] . ' ">' .$values['text']. '</span>';
        }
        return $result;
    }

}