<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

 public function __construct() {
   parent::__construct();
   $this->load->helper ("form");
   $this->load->library ('form_validation');
   $this->load->helper('html_helper');
 } 

 //INDEX
 public function index() {
  $data['site']="Mirana-blog.com";
  $data['contacter']="Nous contacter";
  $data['tel']="tél :  +261325109001";
  $data["title"] = "Page d'accueil";

  $this->load->view('common/header', $data);
  $this->load->view('site/index', $data);
  $this->load->view('common/footer', $data);

}

  //Apropos
  public function apropos() {

    $data['title']='À propos ';
    $data['site']="Mirana-blog.com";
    $data['contacter']="Nous contacter";
    $data['tel']="tél :  +261325109001";

    $this->load->view('common/header', $data);
    $this->load->view('site/apropos', $data);
    $this->load->view('common/footer', $data);
  
  }
  //carte
  public function carte() {

    $data['title']='Carte interactive  ';
    $data['site']="Mirana-blog.com";
    $data['contacter']="Nous contacter";
    $data['tel']="tél :  +261325109001";

    $this->load->view('common/header', $data);
    $this->load->view('site/carte', $data);
    $this->load->view('common/footer', $data);
  
  }

  //connexion
  public function connexion() {
    $data['title']='Se connecter';
    $data['site']="Mirana-blog.com";
    $data['contacter']="Nous contacter";
    $data['tel']="tél :  +261325109001";

    //validation du formulaire
    if ($this->form_validation->run()) {
      $username = $this->input->post('username');
      $password = $this->input->post('password');
      $this->auth_user->login($username, $password);
      //redirection vers la page d'accueil en cas de réussite
      if ($this->auth_user->is_connected) {
        redirect('index');

      } else {
      //affichage en cas d'echec 
        $data['login_error'] = "Échec de l'authentification";
      }
    }
    $this->load->view('common/header', $data);
    $this->load->view('site/connexion', $data);
    $this->load->view('common/footer', $data);
  }

  //CONTACT
  public function contact() {
  
    $data['site']="Mirana-blog.com";
    $data['contacter']="Nous contacter";
    $data['tel']="tél :  +261325109001";
    $data["title"] = "Contact";


    $this->load->view('common/header', $data);
    //Envoyer l'email
    if ($this->form_validation->run()) {
      $this->load->library('email');
      $this->email->from($this->input->post('email'), $this->input->post('name'));
      $this->email->to('mirana@gmail.com');
      $this->email->subject($this->input->post('title'));
      $this->email->message($this->input->post('message'));
    
      if ($this->email->send()) {
        $data['result_class'] = "alert-success";
        $data['result_message'] = "Votre message a été bien envoyé . Nous y répondrons dans les meilleurs délais.";
      } else {
        //affichage en cas de l'envoie réussite
        $data['result_class'] = "alert-danger";
        $data['result_message'] = "Echec de l'envoi . Veuillez-renvoyer ulterieurement votre message.";
        $this->email->clear();
      }
      //Resultat du message en cas de réussite
      $this->load->view('site/resultat_contact', $data);
    } else {
      $this->load->view('site/contact', $data);
    }
    $this->load->view('common/footer', $data);
  }

  //DECONNEXION
  function deconnexion() {
    $this->auth_user->logout();
    redirect('index');
  }

 
}