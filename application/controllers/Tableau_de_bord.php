<?php
defined ('BASEPATH') OR exit ('No direct  script  access  allowed eto ') ;
class Tableau_de_bord extends CI_Controller{

    public function index(){

        parent::__construct();
        $this->load->helper ('form');
        $this->load->library ('form_validation');
        $this->load->helper('html_helper');

        $data['site']="Mirana-blog.com";
        $data['contacter']="Nous contacter";
        $data['tel']="tél :  +261325109001";

        
        if($this->auth_user->is_connected){
            redirect('index');
        }
        

        $data["title"]="Tableau de bord";

        $this->load->view('common/header', $data);
        $this->load->view('Tableau_de_bord/index', $data);
        $this->load->view('common/footer', $data);
    }

    
}