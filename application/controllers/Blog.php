<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

    public function __construct(){
        
        parent::__construct();

        $this->load->helper('date');
        $data['site']="mirana.com";
        $data['title']="Blog";
    }
   
    public function index(){

        $this->load->model('articles');
        $this->load->model('articles_status');
        $this->articles->load($this->auth_user->is_connected);

        //afficher tous les vue
        $this->load->view('common/header',$data);
        $this->load->view('blog/index' ,$data);
        $this->load->view('common/footer' ,$data);



    }
    
}