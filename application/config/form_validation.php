<?php
defined('BASEPATH') OR exit('No direct script  access allowed');

$config = array(
    'site/contact'=>array(
        array(
            'field'=>'name',
            'label'=>'Nom',
            'rules'=>'trim|required|min_length[2]|max_length[250]'
        ),
        array(
            'field'=>'email',
            'label'=>'e-mail',
            'rules'=>array('valid_email','required')
        ),
        array(
            'field'=>'title',
            'label'=>'Titre',
            'rules'=>'trim|required|min_length[2]|max_length[250]'
        ),
        array(
            'field'=>'message',
            'label'=>'Message',
            'rules'=>'trim|required'
        ),
        array(
            'field' => 'emailconf' ,
            'label' => 'Confirmation e-mail' ,
            'rules' => array ('valid_email','required' , 'matches[email]')
        )
    ),    
 
    'site/connexion'=>array(
        array(
            'field'=>'username',
            'label'=>"Non d'utilisateur",
            'rules'=>'required'

        ),
        array(
            'field'=>'password',
            'label'=>'Mot de passe',
            'rules'=>'required'
        )
    )
    
);