<!--CREATION DU FORMULAIRE-->
<div class = "containet">
    <div class="container">
        <div class="row">
            <?=heading ($title); ?>     
        </div>
    </div>    
    <div class="container">
            <div class="row">
                <div class="col col-md-10 col-lg-8 col-xl-8 col-xl-offset-1">
                    <?=form_open('connexion',['class'=>'form-horizontal ']); ?>
                              <!-- DEFINIR UNE ERREUR EN CAS D'ECHE DE LA CONNEXION -->

                            <?php if(!empty($login_error)): ?> 
                                <div class="form-group">
                                    <div class="has-error alert bg-danger">
                                        <span class="help-block"> <?= $login_error ;?> </span>
                                    </div>
                                </div>                             
                            <?php endif; ?>
                              
                            <div class="form-group">
                                <?= form_label("Nom d'utilisateur &nbsp; :" , "username" , ['class'=> "  control-label" ]) ?>
                                <div class=" <?= empty(form_error('username')) ? '':'has-error'  ?>" > 
                                <?= form_input (['name'=> "username" ,'id' => "username",'class'=> 'form-control'],set_value('username')) ?>
                                <span class="help-block"><?=form_error('username');?></span>
                                </div>
                            </div>

                            <div class ="form-group">
                                    <?= form_label("Mot de passe &nbsp; : " , "password" , ['class' => " control-label "]) ?>
                                <div class =" <?= empty(form_error('password')) ?'':'has-error'  ?>" >
                                    <?= form_input (['name'=> "password" ,'id' => "password" ,"type"=>'text','class'=> 'form-control'],set_value('password')) ?>
                                    <span class="help-block"> <?=form_error('password'); ?> </span>
                                </div>
                            </div>

                            <div class="form-group" >
                                <div class ="" >
                                    <?=form_submit("send","Envoyer",['class'=>"btn  btn-outline-info btn-block" ]) ; ?>
                                </div>
                            </div>

                    <?=form_close();?>
                </div>
            </div> 
    </div>
</div>

<script>
    $(document).ready(function(){

       $('#username').focus(function(){
           $(this).css('background-color','blue');
       });
    
       $('#password').focusout(function(){
           $(this).css('background-color','white');
       });
       
    });
</script>