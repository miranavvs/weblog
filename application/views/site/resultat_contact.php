<!--flash de validation ok mail -->
<div class="container">
        <div class="row">
            <?= heading($title); ?>
        </div>
        <hr>
    <div class="col col-md-10 col-md-offset-1">   
        <div class="row alert <?= $result_class; ?>" role="alert">
            <?= $result_message ;?>
         </div>
        <div class="row text-center">
            <?= anchor("index","Fermer" ,['class'=>"btn btn-primary btn-bloc"]); ?>
        </div>
    </div>
</div>
<!-- flash de validation -->