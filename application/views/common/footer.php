
<footer class="footer bd-footer text-muted " id="footer">

    <div class="container-fluid">    
            <div class="row">
              
                <div class="col-lg-2  col-md-2 col-sm-6 col-xs-6 ">
                        <h3> Contacts </h3>
                        <ul>
                            <li> <?=anchor('contact', $contacter);?> </li>
                            <li> <?= $tel;?> </li>
                           
                        </ul>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 ">
                        <h3> News </h3>
                        <ul>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                        </ul>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 ">
                        <h3> Contacts </h3>
                        <ul>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                        </ul>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 ">
                        <h3> Liens </h3>
                        <ul>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                            <li> <a href="# "> Gasy-gasy </a> </li>
                        </ul>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                        <h3> News </h3>
                        <ul>
                            <li>
                                <div class="input-append newsletter-box text-center ">
                                    <input type="text " class="full text-center" placeholder="Email ">
                                    <button class="btn btn-outline-info " type="button "> Envoyer  <i class="fa fa-long-arrow-right "> </i> </button>
                                </div>
                            </li>
                        </ul>
                        <ul class="social">
                            <li>
                                <a href="# "> <i class=" fa fa-facebook ">   </i> </a>
                            </li>
                            <li>
                                <a href="# "> <i class="fa fa-twitter ">   </i> </a>
                            </li>
                            <li>
                                <a href="# "> <i class="fa fa-google-plus ">   </i> </a>
                            </li>
                            <li>
                                <a href="# "> <i class="fa fa-pinterest ">   </i> </a>
                            </li>
                            <li>
                                <a href="# "> <i class="fa fa-youtube ">   </i> </a>
                            </li>
                            <li>
                                <a href="# "> <i class="fa fa-download ">   </i> </a>
                            </li>
                        </ul>
                </div>

            </div>
                <div class="col col-lg-10 text-center">
                        &copy; <?=anchor( base_url(),"$site");?>&nbsp2016-<?=date('Y') ." all rigth reserved" ;?>
                </div>
                <!--/.row-->
    </div><!--/.container-->

</footer>
<a href="#" class="scrollup"><i class="fa fa-angle-up active "></i></a>
<a href="#" class="scrolldown"><i class="fa fa-angle-down active "></i></a>


</body>

        <script src="<?=js_url('bootstrap.min');       ?>"></script>
        <script src="<?=js_url('jquery-3.2.1.min');    ?>"></script>
        <script src="<?=js_url('DATE-HEURE');          ?>"></script>
        <script src="<?=js_url('npm');                 ?>"></script>
        <script src="<?=js_url('popper.min');          ?>"></script>
        <script src="<?=js_url('bootstrap.bundle');    ?>"></script>
        <script src="<?=js_url('scrollTop');           ?>"></script>
        <script src="<?=js_url('classlist');           ?>"></script>
        <script src="<?=js_url('script-sige');         ?>"></script>
 
        <script>
            if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
                var msViewportStyle = document.createElement("style")
                msViewportStyle.appendChild(
                    document.createTextNode(
                        "@-ms-viewport{width:auto!important}"
                    )
                )
                document.getElementsByTagName("head")[0].appendChild(msViewportStyle)
            }
        </script>
</html>