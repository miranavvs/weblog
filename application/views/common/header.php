<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="fr">
 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width , initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?=img_url('computer.png'); ?>">
    <title><?=$title;?></title>

    <link rel="stylesheet" href="<?= base_url("assets/css/bootstrap.min.css");           ?>">
    <link rel="stylesheet" href="<?= base_url("assets/css/bootstrap.min.css");           ?>">
    <link rel="stylesheet" href="<?= base_url("assets/css/bootstrap-reboot.min.css");    ?>">
    <link rel="stylesheet" href="<?= base_url("assets/css/perso.css");                   ?>">
    <link rel="stylesheet" href="<?= base_url("assets/css/vivien.css");                  ?>">
    <link rel="stylesheet" href="<?= base_url("assets/css/font-awesome-4.6.3.min.css");  ?>">
    <link rel="stylesheet" href="<?= base_url("assets/css/background-color.css");        ?>">
    <link rel="stylesheet" href="<?= base_url("assets/css/style-footer.css");            ?>">
    <link rel="stylesheet" href="<?= base_url("assets/css/w3.css");                      ?>">
    <link rel="stylesheet" href="<?= base_url("assets/css/bootstrap-social.css");        ?>">
    <link rel="stylesheet" href="<?= base_url("assets/css/back-to-top.css");             ?>">
    <link rel="stylesheet" href="<?=base_url("assets/css/style-sige.css");               ?>">

  
    <style>
      .image{
         background-image:url(<?= img_url(''); ?>);
         width:100%;
         heogth:50%;
      }
    </style>

 

 </head>
  <body onLoad="getDate()">
    <!-- header content-----   -->
    <header class="image ">
        <div class="container-fluid ">
            <div class="row">                
                <div class="col-md-12 col-lg-12 ">
                    <form class="navbar-form navbar-rigth navbar " name="date">
                        <input class="btn btn-info disabled btn-block" name="mydate" value="">
                    </form>
                </div>
            </div>
        </div>
    </header>
    <!-- /header content ---->    

    <!-- Fixed navbar ------------------------------ NAVBAR -------------------------------------- -->

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="<?=base_url();?>"><?=$site;?></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample03" aria-controls="" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExample03">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item ">
              <?= anchor('index',"<button class='btn btn-outline-info btn-md btn-block'>Accueil</button>");    ?> 
          </li>
          <li class="nav-item">
              <?= anchor('contact',"<button class='btn btn-outline-info btn-md btn-block'>Contact</button>");  ?> 
          </li>
          <li class="nav-item">
             <?= anchor('apropos' ,"<button class='btn btn-outline-info btn-md btn-block'>Apropos</button>"); ?>
          </li>
          <li class="nav-item">
             <?= anchor('carte' ,"<button class='btn btn-outline-info btn-md btn-block'>carte</button>"); ?>
          </li>
          <!-- user identifier seul peut voir ca -->
           
           <?php if($this->auth_user->is_connected): ?>
                <li class="nav-item">
                    <?=anchor ('Tableau_de_bord/index',"<button class='btn-outline-info btn-block'>Tableau de bord</button>"); ?>
                </li>
                <?php endif; ?>
           </li>
          <!--/user -->
          <li class="nav-item">
             <?=anchor ('blog',"<button class='btn btn-outline-info btn-md btn-block'>Blog</button>"); ?>
          </li>
         
        </ul>
        <!--  ------------------    Access Controle ---------------------- -->
        <ul class ="nav navbar-nav  navbar-right">
            <?php if($this->auth_user->is_connected): ?>
                <li class="nav-item"> <?= anchor('Deconnexion',"<button class='btn btn-secondary btn-block btn-sm'>Deconnexion</button>"); ?> </li>
            <?php else: ?>
                <li><?= anchor('connexion',"<button class='btn btn-secondary btn-block btn-sm' id='btn-connecter'>Connexion</button>");?></li>
            <?php endif; ?>
        </ul> 
            <?php if($this->auth_user->is_connected): ?>
                <p class="navbar-text navbar-right">|</p>
                <p class="navbar-text navbar-rigth"><b><?=  $this->auth_user->username ;?></b></p>
            <?php endif; ?>
      </div>
    </nav>

    <script>
      var connexion = document.querySelector("#btn-conneter");
          connexion.title="Connexion";
    </script>