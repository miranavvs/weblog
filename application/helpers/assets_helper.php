<?php
      defined('BASEPATH') OR exit('No direct script access allowed');
         /*--------------------- fonction ajout css ------------------------- */

    if ( ! function_exists('css_url'))
     {
          function css_url($nom)
          {
            return base_url() . 'assets/css/' . $nom . '.css';
           }
      }


         /*--------------------- fonction ajout javascript ------------------------- */
    if ( ! function_exists('js_url'))
     {
          function js_url($nom)
          {
             return base_url() . 'assets/js/' . $nom . '.js';
           }
      }
         /*--------------------- fonction ajout images ------------------------- */
      if ( ! function_exists('img_url'))
      {
           function img_url($nom)
           {
              return base_url() . 'assets/images/' . $nom;
           }
      }
      if ( ! function_exists('img'))
      {
            function img($nom, $alt = '')
          {  
              return '<img src="' . img_url($nom) . '" alt="' . $alt . '" />';
          }
      }


      /*--------------------- fonction ajout video ------------------------- 
      if ( ! function_exists('video_url'))
      {
           function video_url($nom)
           {
               return base_url().'assets/videos/'.$nom;
           }

      }
      if(! function_exists(video)){
          function video($nom,$alt='',$star='false')
          {
              return'<embed src="'.video_url($nom).'"alt="'.$alt.'"autostar="'.$star.'"/>';
          }
      }
*/