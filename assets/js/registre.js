            
/* FONCTION-2 : COLORER LE CHAMP QUI CONTIENT DES ERREURS */

function surligne(champ, erreur)
{
   if(erreur)
      champ.style.backgroundColor = "red";
   else
      champ.style.backgroundColor = "";
      
}            

/* FONCTION-3 TESTER LE PSEUDO */
function verifuser(champ)
{
   if(champ.value.length < 2 || champ.value.length > 25)
   {
      surligne(champ, true);
      return false;
   }  
   else
   {
      surligne(champ, false);
      return true;
   }
}


/* FONCTION -4 TESTER EMAIL */

function verifemail(champ)
{
   var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
   if(!regex.test(champ.value))
   {
      surligne(champ, true);
      return false;
   }
   else
   {
      surligne(champ, false);
      return true;
   }
}

/* FONCTION -5 TESTER L'AGE */

function verifage(champ)
{
   var age = parseInt(champ.value);
   if(isNaN(age) || age < 5 || age > 111)
   {
      surligne(champ, true);
      return false;
   }
   else
   {
      surligne(champ, false);
      return true;
   }
}


/* FONCTION -6 TESTE LES DEUX MOT DE PASSE 

function checkpw(reg) {
 var elt=document.forms['reg'];
 var pw1 = elt.elements['password-1'].value;
 var pw2 = elt.elements['password-2'].value;

 if (pw1 != pw2) {
  alert ("\erreur: les mots de passes ne correspondent pas");
  return false;
 }
else{
 return true;
}
}
*/

/* GLOBALE FONCTION */

function verifForm(f)
 {
   var pseudoOk = verifuser(f.username);
   var mailOk = verifemail(f.email);
   var ageOk = verifage(f.age);
   
   if(pseudoOk && mailOk && ageOk){
      alert("Enregistrés avec succé !");
      return true;
     }   
   else
   {
      alert("Veuillez remplir correctement tous les champs");
      return false;
   }
}

/* -----------------------------AUTRE ----------------------- */



