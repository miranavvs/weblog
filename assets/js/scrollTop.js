//scroll to top
$(document).ready(function() {
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
            $('.scrolldown').fadeOut();
        } else {
            $('.scrollup').fadeOut();
            $('.scrolldown').fadeIn();
        }

    });

    $('.scrollup').click(function() {
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });
    $('.scrolldown').click(function() {
        $("html, body").animate({ scrollTop: 1900 }, 3500);
        return false;
    });


});