var map = document.querySelector('#map');
var paths = map.querySelectorAll('.map_image a');
var links = map.querySelectorAll('.map_list a');
var titre = document.querySelector('#titre');
var link = "<a href='http://google.com?q=Madagascar'><b>Madagascar</b></a>"
titre.innerHTML = link;
var activer = function(id) {

    map.querySelectorAll('.is_active').forEach(function(item) {
        item.classList.remove('is_active');
    })
    if (id !== undefined) {
        document.querySelector('#list-' + id).classList.add('is_active');
        document.querySelector('#region-' + id).classList.add('is_active');
    }

}

paths.forEach(function(path) {
    path.addEventListener('mouseenter', function(e) {
        var id = this.id.replace('region-', '')
        activer(id);

    });
});

links.forEach(function(link) {
    link.addEventListener('mouseenter', function(e) {
        var id = this.id.replace('list-', '')
        activer(id);
    })
})

map.addEventListener('mouseover', function() {
    activer();
})