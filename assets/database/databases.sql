DROP DATABASE IF EXISTS blog_m;
CREATE DATABASE IF NOT EXISTS blog_m DEFAULT CHARACTER SET utf8  
DEFAULT COLLATE utf8_general_ci;
SET default_storage_engine = INNODB;
USE blog_m;
CREATE TABLE login (
    idlog int(10) auto_increment,
    username varchar(32) NOT NULL,
    password varchar(128) NOT NULL,
    status char(1) NOT NULL DEFAULT 'A',
    PRIMARY KEY (idlog),
    UNIQUE KEY (username)
);

CREATE TABLE article(
    idarticle int(10) auto_increment,
    title varchar(100) NOT NULL,
    alias varchar(100) NOT NULL, 
    content text COLLATE latin1_general_ci NOT NULL,
    picture varchar(255) NOT NULL,
    date_create datetime NOT NULL ,
    status  char(1) NOT NULL DEFAULT 'W',
    idlog int(10) NOT NULL,
    PRIMARY KEY(idarticle),
    CONSTRAINT fk_idlog FOREIGN KEY(idlog) REFERENCES login(idlog) ON DELETE RESTRICT ON UPDATE CASCADE
);   

CREATE TABLE user (
    iduser int(10) auto_increment NOT NULL,
    username varchar(100) NOT NULL,
    name varchar(100) NOT NULL,
    photo varchar(255),
    email varchar(255) NOT NULL,
    phone char NOT NULL,
    sexe varchar(6) NOT NULL,
    status char(1) NOT NULL,
    password varchar(32) NOT NULL,
    PRIMARY KEY (iduser)
);  

CREATE TABLE comment (
    idcoment int(10) NOT NULL,
    date_create datetime NOT NULL,
    article int(10) NOT NULL,
    auteur int(10) NOT NULL,
    admin  int(10) NOT NULL,
    PRIMARY KEY (idcoment) ,
    CONSTRAINT fk_auteur  FOREIGN KEY(auteur)  REFERENCES user(iduser) ON DELETE RESTRICT  ON UPDATE CASCADE,
    CONSTRAINT fk_article FOREIGN KEY(article) REFERENCES article(idarticle) ON UPDATE CASCADE  ON DELETE RESTRICT 
);

CREATE TABLE if not EXISTS picture(
    idpicture int(10) auto_increment,
    name varchar(254) NOT NULL,
    date_create datetime NOT NULl,
    date_modif datetime NOT NULl,
    status char(1) NOT NULL DEFAULT '1',
    id_admin int(10) NOT NULL,
    PRIMARY KEY(idpicture),
    CONSTRAINT fk_idadmin FOREIGN KEY(id_admin) REFERENCES login(idlog) ON DELETE RESTRICT ON UPDATE CASCADE
);

INSERT INTO login value(1,'mirana','mirana_01','A');

/* crée une vue article et son auteur */

CREATE OR REPLACE VIEW article_username 
AS 
SELECT  idarticle,
        title,
        alias,
        content,
        picture,
        date_create,
        article.status,
        article.idlog
FROM article
INNER JOIN logiN ON article.idlog = login.idlog;










                